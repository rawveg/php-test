<?php
/**
 * Application: Pokedex for UKFast
 * Author: Tim Green
 * Copyright (c) 2019.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Storage\Psr6CacheStorage;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy;
use Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy;
use stdClass;

/**
 * Class Pokedex
 * @package App
 */
class Pokedex extends Model
{

    const POKEAPI = 'https://pokeapi.co/api/v2/';

    // Cache Results for 10 minutes
    const TTL = 600;

    /** @var \GuzzleHttp\Client  */
    protected $client;

    /** @var \GuzzleHttp\HandlerStack  */
    protected $cacheStack;

    /**
     * Pokedex constructor.
     */
    public function __construct()
    {
        $this->client = new Client(
            self::getClientConfig()
        );
    }

    /**
     * @param array $params
     * @return \stdClass
     */
    public function pokemon(array $params=[]): stdClass
    {
        $queryString = '';
        $endpoint = 'pokemon/';
        if(count($params) > 0)
        {
            $queryString = "?" . http_build_query($params);
            $endpoint.=$queryString;
        }
        return $this->_makeRequest($endpoint);
    }

    /**
     * @param $species
     * @return \stdClass
     */
    public function species($species): stdClass
    {
        //$endpoint = 'pokemon-species/'.$species;
        $endpoint = 'pokemon/'.$species;
        return $this->_makeRequest($endpoint);
    }

    /**
     * @param $endPoint
     * @return \stdClass
     */
    private function _makeRequest($endPoint): stdClass
    {
        $data = new stdClass();
        try {
            $result = $this->client->get(self::POKEAPI . $endPoint);
            $data = json_decode($result->getBody()->getContents());
        } catch (\Exception $e) {
            // do nothing
        }
        return $data;
    }

    /**
     * Get Guzzle Client Cache Config
     *
     * @return array
     */
    static function getClientConfig(): array
    {

        $clientConfig = [];

        $stack = HandlerStack::create();
        $cacheFolderName = 'PokiAPICache';
        $cacheFolderPath = base_path() . "/bootstrap";

        // Instantiate PSR-6 file system cache
        $cache_storage = new Psr6CacheStorage(
            new FilesystemAdapter(
                $cacheFolderName,
                self::TTL,
                $cacheFolderPath
            )
        );

        // Add Cache Method
        $stack->push(
            new CacheMiddleware(
                new GreedyCacheStrategy(
                    $cache_storage,
                    self::TTL
                )
            ),
            'greedy-cache'
        );

        return [
            'handler' => $stack
        ];
    }

}
