<?php
/**
 * Application: Pokedex for UKFast
 * Author: Tim Green
 * Copyright (c) 2019.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class Pokemon
 * @package App\Http\Controllers
 */
class Pokemon extends Controller
{

    /** @var \App\Pokedex  */
    protected $model;

    /**
     * Pokemon constructor.
     * @param \App\Pokedex $model
     */
    public function __construct(
        \App\Pokedex $model
    )
    {
        $this->model = $model;
    }

    public function view($species)
    {
        $speciesData = $this->model->species(strtolower($species));
        return view('pokemon')->with([
            'species'       => $species,
            'speciesData'   => $speciesData
        ]);
    }

}
