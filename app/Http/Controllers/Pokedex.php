<?php
/**
 * Application: Pokedex for UKFast
 * Author: Tim Green
 * Copyright (c) 2019.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class Pokedex
 * @package App\Http\Controllers
 */
class Pokedex extends Controller
{

    /** @var \Illuminate\Http\Request  */
    protected $request;

    /** @var \App\Pokedex  */
    protected $model;

    /**
     * Pokedex constructor.
     * @param \Illuminate\Http\Request $request
     * @param \App\Pokedex $model
     */
    public function __construct(
        \Illuminate\Http\Request $request,
        \App\Pokedex $model
    )
    {
        $this->request = $request;
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        $query = $this->request->post('q');
        $pokemon = [];
        $data = $this->model->pokemon(['limit' => 964]);
        foreach($data->results as $item)
        {
            if(!is_null($query))
            {
                $re = "/.*" . $query . ".*/i";
                if(preg_match($re, $item->name))
                {
                    $pokemon[] = $item->name;
                }
            } else {
                $pokemon[] = $item->name;
            }
        }
        sort($pokemon);
        return view('index')->with([
            'q'         =>  $query,
            'pokemon'   =>  $pokemon
        ]);
    }

}
