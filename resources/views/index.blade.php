<html>
<body>
<h1>Pokédex</h1>
<div class="container">
    <div class="search">
        <form action="/" method="POST" role="search">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="text" class="form-control" name="q" value="{{$q}}" placeholder="Search Pokédex">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                        Search
                    </button>
                    <button type="reset" class="btn" onclick="window.location.href='/';">
                        Clear
                    </button>
                </span>
            </div>
        </form>
    </div>
    @if(count($pokemon) > 0)
        @foreach($pokemon as $poke)
        <div>
            <a href="/pokemon/{{$poke}}">{{ucfirst($poke)}}</a>
        </div>
        @endforeach
    @else
        <p><strong>Your search has produced no results</strong></p>
    @endif
</div>
</body>
</html>
