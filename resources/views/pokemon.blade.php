<html>
<head>
    <title>{{ucfirst($species)}}</title>
    <style>
        div.images {
            display: table;
        }
        div.sprites {
            display: table-cell;
            text-align: center;
        }
    </style>
</head>
<body>
<h1>{{ucfirst($species)}}</h1>
<div class="content">
    <h2>Images</h2>
    <div class="images">
        @foreach($speciesData->sprites as $imgtype=>$value)
            @if(!is_null($value))
            <div class="sprites">
                <img src="{{$value}}" title="{{$imgtype}}" /><br />
                {{$imgtype}}
            </div>
            @endif
        @endforeach
    </div>
    <h2>Profile</h2>
    <p>
        <strong>Abilties: </strong>
        @foreach($speciesData->abilities as $ability)
            {{$ability->ability->name}},&nbsp;
        @endforeach
    </p>
    <p>
        <strong>Base Experience: </strong>{{$speciesData->base_experience}}
    </p>
    <p>
        <strong>Species: </strong>{{ucfirst($speciesData->species->name)}}
    </p>
    <p>
        <strong>Height: </strong>{{$speciesData->height}}
    </p>
    <p>
        <strong>Weight: </strong>{{$speciesData->weight}}
    </p>
    <p>
        <strong>Moves: </strong>
        @foreach($speciesData->moves as $moves)
            {{$moves->move->name}},&nbsp;
        @endforeach
    </p>
    <p>
        <a href="/"><<< Back</a>
    </p>
</div>
</body>
</html>
